/*
Ethan Moscot
CSE 002
HW 04
This program prints out the slang terminology within the game Craps using if statements. 
*/
import java.util.Scanner;
public class CrapsIf {
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    int d1;
    int d2;
    int choice;
    int totalRoll;
    String slang = "";
    System.out.print("Press 1 to randomly cast two dice or press 2 to enter your own numbers: ");
    choice = input.nextInt();
    if(choice == 1) { //option to randomly cast two die
      d1 = (int)(Math.random() * 6) + 1;
      d2 = (int)(Math.random() * 6) + 1;
      totalRoll = d1 + d2;
      if(totalRoll == 2) { //Roll 2
        slang = "Snake Eyes";
      } else if(totalRoll == 3) { //Roll 3
        slang = "Ace Deuce";
      } else if(totalRoll == 4) { //Roll 4
        if(d1 == 3 || d1 == 1) {
        slang = "Easy Four";
        } else {
        slang = "Hard Four";
        }
      } else if(totalRoll == 5) { //Roll 5
        slang = "Fever Five";
      } else if(totalRoll == 6) { //Roll 6
        if(d1 == 3) {
          slang = "Hard Six";
        } else {
          slang = "Easy Six";
        }
      } else if(totalRoll == 7) { //Roll 7
        slang = "Seven Out";
      } else if(totalRoll == 8) { //Roll 8
        if(d1 == 8) {
          slang = "Hard Eight";
        } else {
          slang = "Easy Eight";
        }
      } else if(totalRoll == 9) { //Roll 9
        slang = "Nine";
      } else if(totalRoll == 10) { //Roll 10
        if(d1 == 10) {
          slang = "Hard Ten";
        } else {
          slang = "Easy Ten";
        }
      } else if(totalRoll == 11) { //Roll 11
        slang = "Yo-leven";
      } else if(totalRoll == 12) { //Roll 12
        slang = "Boxcars";
      }
      System.out.println(slang + '!');
    } else if(choice == 2) { //user enters numbers for two die
      System.out.print("Enter the first die (1-6): ");
      d1 = input.nextInt();
      System.out.print("Enter the second die (1-6): ");
      d2 = input.nextInt();
      totalRoll = d1 + d2;
      if(totalRoll == 2) { //Roll 2
        slang = "Snake Eyes";
      } else if(totalRoll == 3) { //Roll 3
        slang = "Ace Deuce";
      } else if(totalRoll == 4) { //Roll 4
        if(d1 == 3 || d1 == 1) {
        slang = "Easy Four";
        } else {
        slang = "Hard Four";
        }
      } else if(totalRoll == 5) { //Roll 5
        slang = "Fever Five";
      } else if(totalRoll == 6) { //Roll 6
        if(d1 == 3) {
          slang = "Hard Six";
        } else {
          slang = "Easy Six";
        }
      } else if(totalRoll == 7) { //Roll 7
        slang = "Seven Out";
      } else if(totalRoll == 8) { //Roll 8
        if(d1 == 8) {
          slang = "Hard Eight";
        } else {
          slang = "Easy Eight";
        }
      } else if(totalRoll == 9) { //Roll 9
        slang = "Nine";
      } else if(totalRoll == 10) { //Roll 10
        if(d1 == 10) {
          slang = "Hard Ten";
        } else {
          slang = "Easy Ten";
        }
      } else if(totalRoll == 11) { //Roll 11
        slang = "Yo-leven";
      } else if(totalRoll == 12) { //Roll 12
        slang = "Boxcars";
      }
      System.out.println(slang + '!');
    } else { //Checks if user enters correct integer for either option 1 or 2
      System.out.println("You have entered an invalid option. Please try again.");
    }
  }
}