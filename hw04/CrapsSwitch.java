/*
Ethan Moscot
CSE 002
HW 04
This program prints out the slang terminology within the game Craps using switch statements. 
*/
import java.util.Scanner;
public class CrapsSwitch {
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    int d1; //First die
    int d2; //Second die
    int choice; //Stores user choice
    int totalRoll; //Stores result of two dice added together
    String slang = ""; //Stores slang
		//User input
    System.out.print("Press 1 to randomly cast two dice or press 2 to enter your own numbers: ");
    choice = input.nextInt();
    switch(choice) { //Either computes random dice values or reads in user's values based on input
			case 1: //Randomized selection
				//Randomize two dice
				d1 = (int)(Math.random() * 6) + 1;
				d2 = (int)(Math.random() * 6) + 1;
				totalRoll = d1 + d2;
				switch(totalRoll) {
					case 2: //Total roll = 2
						slang = "Snake Eyes";
						break;
					case 3: //Total roll = 3
						slang = "Ace Deuce";
						break;
					case 4: //Total roll = 4
						switch(d1) {
							case 2:
								slang = "Hard Four";
							default:
								slang = "Easy Four";
						}
						break;
					case 5: //Total roll = 5
						slang = "Fever Five";
						break;
					case 6: //Total roll = 6
						slang = "Easy Six";
						switch(d1) {
							case 3:
								slang = "Hard Six";
						}
						break;
					case 7: //Total roll = 7
						slang = "Seven Out";	
						break;
					case 8: //Total roll = 8
						switch(d1) {
							case 4:
								slang = "Hard Eight";
							default: 
								slang = "Easy Eight";
						}	
						break;
					case 9: //Total roll = 9
						slang = "Nine";
						break;
					case 10: //Total roll = 10
						switch(d1) {
							case 5:
								slang = "Hard Ten";
							default:
								slang = "Easy Ten";
						}
						break;	
					case 11: //Total roll = 11
						slang = "Yo-leven";
						break;
					case 12: //Total roll = 12
						slang = "Boxcars";
						break;
				}
				//Print out slang result to the console
				System.out.println(slang + '!');
				break;
			case 2: //User input case
				//Receive input for two dice
				System.out.print("Enter the first number: ");
				d1 = input.nextInt();
				System.out.print("Enter the second number: ");
				d2 = input.nextInt();
				totalRoll = d1 + d2;
				switch(totalRoll) {
					case 2: //Total roll = 2
						slang = "Snake Eyes";
						break;
					case 3: //Total roll = 3
						slang = "Ace Deuce";
						break;
					case 4: //Total roll = 4
						switch(d1) {
							case 2:
								slang = "Hard Four";
							default:
								slang = "Easy Four";
						}
						break;
					case 5: //Total roll = 5
						slang = "Fever Five";
						break;
					case 6: //Total roll = 6
						slang = "Easy Six";
							switch(d1) {
								case 3:
									slang = "Hard Six";
							}
						break;
					case 7: //Total roll = 7
						slang = "Seven Out";	
						break;
					case 8: //Total roll = 8
						switch(d1) {
							case 4:
								slang = "Hard Eight";
							default: 
								slang = "Easy Eight";
						}	
						break;
					case 9: //Total roll = 9
						slang = "Nine";
						break;
					case 10: //Total roll = 10
						switch(d1) {
							case 5:
								slang = "Hard Ten";
							default:
								slang = "Easy Ten";
						}
						break;	
					case 11: //Total roll = 11
						slang = "Yo-leven";
						break;
					case 12: //Total roll = 12
						slang = "Boxcars";
						break;
				}
				//Print out slang result to the console
				System.out.println(slang + '!');
				break;
			default: //Checks if user enters correct integer for either option 1 or 2
				System.out.println("You have entered an invalid option. Please try again.");
		}
	}
}