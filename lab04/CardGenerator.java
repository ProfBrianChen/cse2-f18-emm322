/*
Ethan Moscot
CSE 002
Lab 04
This program uses random numbers to pick a random card from a card deck. 
*/
public class CardGenerator {
  public static void main(String[] args) {
    int cardNum; //Holds randomly-generated card number
    String suit; //Holds the card's suit
    String identity; //Stores the card's value
    
    //Generate random number from 1-52 inclusive
    cardNum = (int)(Math.random() * 52) + 1;  
    if(cardNum <= 13) { //1-13 = diamonds
      suit = "Diamonds";
      switch(cardNum) { //Assigns card identity
        case 1:
          identity = "Ace";
        case 11:
          identity = "Jack";
        case 12:
          identity = "Queen";
        case 13:
          identity = "King";
        default: 
          identity = cardNum + "";
       }
    } else if(cardNum >= 14 && cardNum <= 26) { //14-26 = clubs
      suit = "Clubs";
      cardNum -= 13; //Set from 1-13
      switch(cardNum) { //Assigns card identity
        case 1:
          identity = "Ace";
        case 11:
          identity = "Jack";
        case 12:
          identity = "Queen";
        case 13:
          identity = "King";
        default: 
          identity = cardNum + "";
      }
    } else if(cardNum >= 27 && cardNum <= 39) { //27-39 = hearts
      suit = "Hearts";
      cardNum -= 26; //Set from 1-13
      switch(cardNum) { //Assigns card identity
        case 1:
          identity = "Ace";
        case 11:
          identity = "Jack";
        case 12:
          identity = "Queen";
        case 13:
          identity = "King";
        default: 
          identity = cardNum + "";
      }
    } else { //40-52 = spades
      suit = "Spades";
      cardNum -= 39; //Set from 1-13
      switch(cardNum) { //Assigns card identity
        case 1:
          identity = "Ace";
        case 11:
          identity = "Jack";
        case 12:
          identity = "Queen";
        case 13:
          identity = "King";
        default: 
          identity = cardNum + "";
      }
    }
    //Print out result to console
    System.out.println("You picked the " + identity + " of " + suit + '.');
  }
}