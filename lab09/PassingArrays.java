/*
Ethan Moscot
CSE 002
Lab 09
11/16/18
*/
public class PassingArrays {
	public static void main(String[] args) {
		int[] array0 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		int[] array1 = copy(array0);
		int[] array2 = copy(array0);

		//invert arrays
		inverter(array0);
		inverter2(array1);
		int[] array3 = inverter2(array2);

		//print out modified arrays
		System.out.println("array0 passed into inverter():");
		print(array0);
		System.out.println("\narray1 passed into inverter2():");
		print(array1);
		System.out.println("\nContents of array3 (array2 passed into inverter2() and assigned to array3): ");
		print(array3);
	}

	//creates a copy of an array
	public static int[] copy(int[] array) {
		int[] copyArray = new int[array.length];
		for(int i = 0; i < array.length; i++) {
			copyArray[i] = array[i];
		}
		return copyArray;
	}

	//inverts/reorders an array
	public static void inverter(int[] array) {
		for(int i = 0; i < array.length / 2; i++) {
			int temp = array[i];
			array[i] = array[array.length - i - 1];
			array[array.length - i - 1] = temp;
		}
	}

	//creates copy of array and inverts it
	public static int[] inverter2(int[] array) {
		int[] copy = copy(array);
		inverter(copy);
		return copy;
	}

	//prints out the array's contents
	public static void print(int[] array) {
		for(int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
	}
}