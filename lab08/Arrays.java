import java.util.Random;
public class Arrays {
	public static void main(String[] args) {
		int[] array = new int[100];
		int[] countArr = new int[100];

		System.out.print("The array contains the integers: ");
		//Fill first array with 100 random integers within the range 0-99
		for(int i = 0; i < array.length; i++) {
			array[i] = (int)(Math.random() * 100);
			System.out.print("" + array[i] + " ");
			countArr[array[i]]++;
		}
		System.out.println();
		//Checks for number of occurrences in original array
		for(int j = 0; j < countArr.length; j++) {
			if(countArr[j] > 1) { //Prints occurrence only if the number is in the array
				System.out.println(j + " occurs " + countArr[j] + " times.");
			} else if(countArr[j] == 1) {
				System.out.println(j + " occurs 1 time.");
			}
		}
	}
}