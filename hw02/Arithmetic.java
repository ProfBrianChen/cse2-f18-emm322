/*
Ethan Moscot
CSE 002
HW 02
This program calculates the prices of various hypothetical items before and after PA sales tax is applied. 
*/
public class Arithmetic {
  public static void main(String[] args) {
    int numPants = 3; //number of pants
    double pantsPrice = 34.98; //price of a pair of pants
    int numShirts = 2; //number of shirts
    double shirtsPrice = 24.99; //price of a shirt
    int numBelts = 1; //number of belts
    double beltPrice = 33.99; //price of a belt
    double paSalesTax = 0.06; //sales tax in PA
    double pantsCost = numPants * pantsPrice; //total price of pants
    double shirtsCost = numShirts * shirtsPrice; //total price of shirts
    double beltCost = numBelts * beltPrice; //total price of belt
    double totalItemCost = pantsCost + shirtsCost + beltCost; //holds price of all items before tax
    double pantsSalesTax = pantsCost * paSalesTax; //sales tax on pants
    double shirtsSalesTax = shirtsCost * paSalesTax; //sales tax on shirts
    double beltSalesTax = beltCost * paSalesTax; //sales tax on belt
    double totalSalesTax = pantsSalesTax + shirtsSalesTax + beltSalesTax; //adds sales tax for all items
    double transactionPrice = totalItemCost + totalSalesTax; //price of transaction with sales tax included
    
    //Cost of pants before tax
    System.out.println("The cost of " + numPants + " pairs of pants priced at $" + pantsPrice + " each = $" + pantsCost);
    //Cost of shirts before tax
    System.out.println("The cost of " + numShirts + " shirts priced at $" + shirtsPrice + " each = $" + shirtsCost);
    //Cost of belt before tax
    System.out.println("The cost of " + numBelts + " belt priced at $" + beltPrice + " = $" + beltCost);
    //Cost of all items before tax
    System.out.println("The total of all items before PA sales tax = $" + totalItemCost);
    
    //Format sales tax charged for pants to only two places after the decimal
    pantsSalesTax *= 100;
    pantsSalesTax = (int)pantsSalesTax;
    pantsSalesTax /= 100;
    //Display sales tax charged for pants
    System.out.println("\nThe sales tax applied to $" + pantsCost + " worth of pants = $" + pantsSalesTax);
    
    //Format sales tax charged for shirts to only two places after the decimal
    shirtsSalesTax *= 100;
    shirtsSalesTax = (int)shirtsSalesTax;
    shirtsSalesTax /= 100;
    //Display salex tax charged for shirts
    System.out.println("The sales tax applied to $" + shirtsCost + " worth of shirts = $" + shirtsSalesTax);
    
    //Format sales tax charged for belt to only two places after the decimal
    beltSalesTax *= 100;
    beltSalesTax = (int)beltSalesTax;
    beltSalesTax /= 100;
    //Display sales tax charged for belt
    System.out.println("The sales tax applied to $" + beltCost + " worth of belts = $" + beltSalesTax);
   
    //Format sales tax to only two places after the decimal
    totalSalesTax *= 100;
    totalSalesTax = (int)totalSalesTax;
    totalSalesTax /= 100;
    //Display total sales tax for all items combined
    System.out.println("The PA sales tax applied to all items combined = $" + totalSalesTax);
    
    //Format total transaction price to only two places after the decimal
    transactionPrice *= 100;
    transactionPrice = (int)transactionPrice;
    transactionPrice /= 100;
    //Display total price of transaction including sales tax
    System.out.println("\nIncluding sales tax, the total price of the transaction = $" + transactionPrice);
  }
}