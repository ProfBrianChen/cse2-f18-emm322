/*
Ethan Moscot
CSE 002
10/26/18
Lab 07
 */
import java.util.*;
public class Methods {
	public static void main(String[] args) {
		char newParagraph = 'y';
		Scanner input = new Scanner(System.in);
		//Creates new paragraph as long as user enters 'y'
		while(newParagraph == 'y') {
			System.out.println(paragraph());
			System.out.print("Enter 'y' for another paragraph or enter any other character to quit: ");
			newParagraph = input.next().charAt(0);
		}
	}

	//Chooses adjectives for sentences from list of 10 adjectives
	public static String adjectives() {
		String word = "";
		int rand = (int)(Math.random() * 10);
		switch(rand) {
		case 0:
			word = "fast";
			break;
		case 1:
			word = "bald";
			break;
		case 2:
			word = "large";
			break;
		case 3:
			word = "angry";
			break;
		case 4:
			word = "scruffy";
			break;
		case 5:
			word = "attractive";
			break;
		case 6:
			word = "skinny";
			break;
		case 7:
			word = "kind";
			break;
		case 8:
			word = "clumsy";
			break;
		}
		return word;
	}

	//Chooses subject nouns for sentences from list of 10 nouns
	public static String subjectNouns() {
		String word = "";
		int rand = (int)(Math.random() * 10);
		switch(rand) {
		case 0:
			word = "man";
			break;
		case 1:
			word = "woman";
			break;
		case 2:
			word = "dog";
			break;
		case 3:
			word = "cat";
			break;
		case 4:
			word = "weightlifter";
			break;
		case 5:
			word = "programmer";
			break;
		case 6:
			word = "college student";
			break;
		case 7:
			word = "professor";
			break;
		case 8:
			word = "president";
			break;
		case 9:
			word = "boss";
			break;
		}
		return word;
	}

	//Chooses past-tense verbs for sentences from list of 10 verbs
	public static String pastTenseVerbs() {
		String word = "";
		int rand = (int)(Math.random() * 10);
		switch(rand) {
		case 0:
			word = "jumped";
			break;
		case 1:
			word = "wrote";
			break;
		case 2:
			word = "flew";
			break;
		case 3:
			word = "ran";
			break;
		case 4:
			word = "walked";
			break;
		case 5:
			word = "studied";
			break;
		case 6:
			word = "danced";
			break;
		case 7:
			word = "carried";
			break;
		case 8:
			word = "mentioned";
			break;
		case 9:
			word = "stole";
			break;
		}
		return word;
	}

	//Chooses object nouns for sentences from list of 10 nouns
	public static String objectNouns() {
		String word = "";
		int rand = (int)(Math.random() * 10);
		switch(rand) {
		case 0:
			word = "trucks";
			break;
		case 1:
			word = "pans";
			break;
		case 2:
			word = "boxes";
			break;
		case 3:
			word = "phones";
			break;
		case 4:
			word = "computers";
			break;
		case 5:
			word = "jackets";
			break;
		case 6:
			word = "shoes";
			break;
		case 7:
			word = "papers";
			break;
		case 8:
			word = "calculators";
			break;
		case 9:
			word = "cars";
			break;
		}
		return word;
	}

	//Formats sentence structure by calling adjective, verb and noun methods
	public static String sentence(String subjectNoun) {
		return "The " + adjectives() + " " + subjectNoun + " " + pastTenseVerbs() + " the " + objectNouns() + ".";
	}

	//Writes conclusion sentence having the subject noun passed in
	public static String conclusion(String noun) {
		return "Finally, the " + noun + " " + pastTenseVerbs() + " the " + objectNouns() + "!";
	}

	//Formats paragraph structure using the subject noun
	public static String paragraph() {
		String subNouns = subjectNouns();
		return sentence(subNouns) + "\n" + sentence(subNouns) + "\n" + conclusion(subNouns);
	}
}