import java.util.Scanner;
public class TicTacToe {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		char board[][] = new char[3][3]; //initialize tic-tac-toe board
		int xRow, xCol, oRow, oCol; //accept row and column move data
		int moveCounter = 0; //records the number of moves
		fillBoard(board); //creates the initial game board
		displayBoard(board); //displays board filled with numbers

		game: //while loop label
			while(!winner(board)) { //run the game as long as no one has won
				xRow = getXRow(); //get 'X' row value
				xCol = getXCol(); //get 'X' column value
				while(board[xRow][xCol] == 'X' || board[xRow][xCol] == 'O') { //check if the entered row and column have already been entered
					System.out.println("The row and column you entered is already occupied. Please repeat the process for an empty slot on the board.");
					xRow = getXRow();
					xCol = getXCol();
				}

				board[xRow][xCol] = 'X'; //draw 'X'
				displayBoard(board); //display updated board
				if(winner(board)) { //check if Player 1 won
					System.out.println("Player 1 has won the game!");
					break game;
				}
				System.out.println();

				moveCounter++; //count move

				if(moveCounter == 9) { //Player 1 moves on 9th turn, so this prints and breaks out of loop if draw
					System.out.println("The game has ended in a draw.");
					break game;
				}

				oRow = getORow(); //get 'O' row value
				oCol = getOCol(); //get 'O' column value

				while(board[oRow][oCol] == 'X' || board[oRow][oCol] == 'O') { //check if the entered row and column have already been entered
					System.out.println("The row and column you entered is already occupied. Please repeat the process for an empty slot on the board.");
					oRow = getORow();
					oCol = getOCol();
				}

				board[oRow][oCol] = 'O'; //draw 'O'
				displayBoard(board); //display updated board
				if(winner(board)) { //check if Player 2 has won
					System.out.println("Player 2 has won the game!");
					break game;
				}

				moveCounter++; //count move
			}
	}
	//Fills the board with incrementing numbers from 1-9
	public static void fillBoard(char[][] board) {
		char square = '1';
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				board[i][j] = square++;
			}
		}
	}

	//Displays the current board separated by pipes
	public static void displayBoard(char[][] board) {
		for(int i = 0; i < board.length; i++) {
			for(int j = 0; j < board[i].length; j++) {
				if(j == board[i].length - 1) {
					System.out.print(board[i][j]);
				}
				else {
					System.out.print(board[i][j] + " | ");
				}
			}
			System.out.println();
		}
	}

	//Prompts user for the row value of the 'X' and completes necessary checks
	public static int getXRow() {
		Scanner input = new Scanner(System.in);
		int xRow;
		System.out.print("Enter the row to place the 'X' in (0-2): ");
		while(!input.hasNextInt()) {
			System.out.print("You did not enter an integer. Please re-enter the row to place the 'X' in (0-2): ");
			input.next();
		}
		xRow = input.nextInt();

		while(xRow > 2 || xRow < 0) {
			System.out.print("Your input is out of bounds. Please re-enter the row to place the 'X' in (0-2): ");
			xRow = input.nextInt();
		}
		return xRow;
	}

	//Prompts user for the column value of the 'X' and completes necessary checks
	public static int getXCol() {
		Scanner input = new Scanner(System.in);
		int xCol;
		//Enter and validate position of 'X' column placement
		System.out.print("Enter the column to place the 'X' in (0-2): ");
		while(!input.hasNextInt()) {
			System.out.print("You did not enter an integer. Please re-enter the column to place the 'X' in (0-2): ");
			input.next();
		}
		xCol = input.nextInt();

		while(xCol > 2 || xCol < 0) {
			System.out.print("Your input is out of bounds. Please re-enter the column to place the 'X' in (0-2): ");
			xCol = input.nextInt();
		}
		return xCol;
	}

	//Prompts user for the row value of the 'O' and completes necessary checks
	public static int getORow() {
		Scanner input = new Scanner(System.in);
		int oRow;
		//Enter and validate position of 'O' row placement
		System.out.print("Enter the row to place the 'O' in (0-2): ");
		while(!input.hasNextInt()) {
			System.out.print("You did not enter an integer. Please re-enter the row to place the 'O' in (0-2): ");
			input.next();
		}
		oRow = input.nextInt();

		while(oRow > 2 || oRow < 0) {
			System.out.print("Your input is out of bounds. Please re-enter the row to place the 'O' in (0-2): ");
			oRow = input.nextInt();
		}
		return oRow;
	}

	//Prompts user for the column value of the 'O' and completes necessary checks
	public static int getOCol() {
		Scanner input = new Scanner(System.in);
		int oCol;
		//Enter and validate position of 'O' column placement
		System.out.print("Enter the column to place the 'O' in (0-2): ");
		while(!input.hasNextInt()) {
			System.out.print("You did not enter an integer. Please re-enter the column to place the 'O' in (0-2): ");
			input.next();
		}
		oCol = input.nextInt();

		while(oCol > 2 || oCol < 0) {
			System.out.print("Your input is out of bounds. Please re-enter the column to place the 'O' in (0-2): ");
			oCol = input.nextInt();
		}
		return oCol;
	}

	//Evaluates all possible winning positions, checks if true, and returns boolean
	public static boolean winner(char[][] board){
		return (board[0][0] == board[0][1] && board[0][0] == board [0][2]) ||
				(board[0][0] == board [1][1] && board[0][0] == board [2][2]) ||
				(board[0][0] == board [1][0] && board[0][0] == board [2][0]) ||
				(board[2][0] == board [2][1] && board[2][0] == board [2][2]) ||
				(board[2][0] == board [1][1] && board[0][0] == board [0][2]) ||
				(board[0][2] == board [1][2] && board[0][2] == board [2][2]) ||
				(board[0][1] == board [1][1] && board[0][1] == board [2][1]) ||
				(board[1][0] == board [1][1] && board[1][0] == board [1][2]);
	}
}