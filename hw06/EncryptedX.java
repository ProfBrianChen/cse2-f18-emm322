/*
Ethan Moscot
CSE 002 HW 06
10/23/18
*/
import java.util.Scanner;
public class EncryptedX {
  public static void main(String[] args) {
    int squareSize;
    Scanner input = new Scanner(System.in);
    //Accepts and validates user input
    System.out.print("Enter an integer from 0-100: ");
    while(!input.hasNextInt()) {
      System.out.print("The input is not an integer. Reenter an integer from 0-100: ");
      input.next();
    }
    squareSize = input.nextInt();
    //Checks if input is between 0-100
    while(squareSize < 0 || squareSize > 100) {
      System.out.print("The input is not within the range. Reenter from 0-100: ");
      squareSize = input.nextInt();
    }

		squareSize += 1; //would set 10x10 to 11x11 matrix as in homework assignment description
    for(int i = 0; i < squareSize; i++) { //loops through row sizes
      for(int j = 0; j < squareSize; j++) { //loops through individual row
        if(i == j || i == squareSize - j - 1) { //prints space if row matches row counter
          System.out.print(" ");
        } else {
          System.out.print("*");
        }
      }
      System.out.println();
    }
  }
}