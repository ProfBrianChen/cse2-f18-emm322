import java.util.*;
public class Shuffling {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		//suits club, heart, spade or diamond
		String[] suitNames = {"C","H","S","D"};
		String[] rankNames = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"};
		String[] cards = new String[52];
		String[] hand = new String[5];
		int numCards = 5;
		int again = 1;
		int index = 51;
		for(int i = 0; i < 52; i++) {
			cards[i] = rankNames[i % 13] + suitNames[i / 13];
			System.out.print(cards[i] + " ");
		}
		System.out.println();
		printArray(cards);
		shuffle(cards);
		System.out.println("Shuffled hand:");
		printArray(cards);
		while(again == 1) {
			hand = getHand(cards,index,numCards);
			printArray(hand);
			index -= numCards;
			System.out.println("Enter a 1 if you want another hand drawn: ");
			again = scan.nextInt();
		}
	}

	//Prints out each element in array separated by space
	public static void printArray(String[] list) {
		for(int i = 0; i < list.length; i++) {
			System.out.print(list[i] + " ");
		}
		System.out.println();
	}

	/*
	 * Shuffles the elements of the list by continuously randomizing an index number of list (that is not zero)
	 * and swaps the element at that index with the first element (at index 0).
	 */
	public static void shuffle(String[] list) {
		int counter = 0;
		while(counter <= 55) { // greater than 50 to ensure the deck is well-shuffled
			int randomIndex = (int)(Math.random() * 51 + 1);
			String temp = list[0];
			list[0] = list[randomIndex];
			list[randomIndex] = temp;
			counter++;
		}
	}

	/*
	 * Returns an array that holds the number of cards specified in numCards.
	 * Cards should be taken off at the end of the list of cards.
	 * If numCards is greater than the number of cards in the deck, create a new deck of cards.
	 */
	public static String[] getHand(String[] list, int index, int numCards) {
		String[] hand = new String[5];
		for(int i = 0; i < 5; i++) {
			hand[i] = list[index - i];
		}
		return hand;
	}
}