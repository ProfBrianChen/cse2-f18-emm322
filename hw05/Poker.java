/*
Ethan Moscot
CSE 002
HW 05
Poker.java
This program calculates the probability of a four-of-a-kind, three-of-a-kind,
two-pair, and single pair within a number of hands that is entered by the user.
*/
import java.util.Scanner;
public class Poker {
  public static void main(String[] args) {
    int card1, card2, card3, card4, card5, numHands = 0;
    int fourOfAKind = 0, threeOfAKind = 0, twoPair = 0, onePair = 0;
    Scanner input = new Scanner(System.in);

    //Checks if input matches int, and accepts number of hands to generate
    System.out.print("Enter the number of hands to generate: ");
		while(!input.hasNextInt()) {
			System.out.print("Invalid input. Reenter the number of hands to generate: ");
			input.next();
		}
		numHands = input.nextInt();

    //Generates hands as set by user
    for(int i = 0; i < numHands; i++) {
      //Randomly generates five cards
      card1 = (int)(Math.random() * 52 + 1);
      card2 = (int)(Math.random() * 52 + 1);
      card3 = (int)(Math.random() * 52 + 1);
      card4 = (int)(Math.random() * 52 + 1);
      card5 = (int)(Math.random() * 52 + 1);
      //Regenerate card 1 if equal to another card
      while(card1 == card2 || card1 == card3 || card1 == card4 || card1 == card5) {
        card1 = (int)(Math.random() * 52 + 1);
      }
      //Regenerate card 2 if needed
      while(card2 == card1 || card2 == card3 || card2 == card4 || card2 == card5) {
        card2 = (int)(Math.random() * 52 + 1);
      }
      //Regenerate card 3 if needed
      while(card3 == card1 || card3 == card2 || card3 == card4 || card3 == card5) {
        card3 = (int)(Math.random() * 52 + 1);
      }
      //Regenerate card 4 if needed
      while(card1 == card2 || card1 == card3 || card1 == card4 || card1 == card5) {
        card4 = (int)(Math.random() * 52 + 1);
      }
      //Regenerate card 5 if needed
      while(card1 == card2 || card1 == card3 || card1 == card4 || card1 == card5) {
        card5 = (int)(Math.random() * 52 + 1);
      }
      //Set cards to range between 1-14 for comparison purposes
      card1 = card1 % 14;
      card2 = card2 % 14;
      card3 = card3 % 14;
      card4 = card4 % 14;
      card5 = card5 % 14;

      //full house logic: three-of-a-kind and a pair
      if(((card1 == card2 && card1 != card4 && card4 != card5) || (card2 == card4 && card1 != card2 && card1 != card5) || (card3 == card5 && card2 != card3 && card2 != card1)) && ((card1 == card2) || (card1 == card3) || (card1 == card4) || (card1 == card5) || (card2 == card3) || (card2 == card4) || (card2 == card5) || (card3 == card4) || (card3 == card5) || (card4 == card5))) {
        threeOfAKind++;
        onePair++;
      }
      //four-of-a-kind logic
      else if((card2 == card3 && card3 == card4) && (card1 == card2 || card5 == card2)) {
        fourOfAKind++;
      }
      //three-of-a-kind logic
      else if((card1 == card2 && card1 != card4 && card4 != card5) || (card2 == card4 && card1 != card2 && card1 != card5) || (card3 == card5 && card2 != card3 && card2 != card1)) {
        threeOfAKind++;
      }
      //two-pair logic
      else if((card1 == card2 && card3 == card4 && card1 != card3) || (card2 == card3 && card4 == card5 && card2 != card4)) {
        twoPair++;
      }
      //one-pair logic
      else if((card1 == card2) || (card1 == card3) || (card1 == card4) || (card1 == card5) || (card2 == card3) || (card2 == card4) || (card2 == card5) || (card3 == card4) || (card3 == card5) || (card4 == card5)) {
        onePair++;
      }
    }
    //Print out stats to the console
    System.out.println("\nThe number of loops: " + numHands);
    System.out.println("The probability of a Four-of-a-kind: " + ((double) fourOfAKind) / numHands);
    System.out.println("The probability of a Three-of-a-kind: " + ((double) threeOfAKind) / numHands);
    System.out.println("The probability of a Two-pair: " + ((double) twoPair) / numHands);
    System.out.println("The probability of a One-pair: " + ((double) onePair) / numHands);
  }
}