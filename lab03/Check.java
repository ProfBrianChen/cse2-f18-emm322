/*
Ethan Moscot
CSE 002
Lab 03
This assignment determines the price each individual pays after splitting a check.
*/
import java.util.Scanner;
public class Check {
  public static void main(String[] args) {
    double totalCost; //holds the total cost of the meal
    double costPerPerson; //The cost of meal per person
    int dollars, dimes, pennies; //stores digits for the cost
    Scanner input = new Scanner(System.in);
    //User enters total cost of check
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    double checkCost = input.nextDouble();
    //User enters desired tip percentage
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = input.nextDouble();
    tipPercent /= 100; //Convert percentage into decimal
    //User enters the number of people who dined out
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = input.nextInt();
    
    totalCost = checkCost * (1 + tipPercent); //Calculates total cost of meal including tip
    costPerPerson = totalCost / numPeople; //Calculates individual cost
    dollars = (int)costPerPerson; //Converts costPerPerson to whole amount
    dimes = (int)(costPerPerson * 10) % 10; //Calculates number of dimes
    pennies = (int)(costPerPerson * 100) % 10; //Calculates number of pennies
    //Displays the calculated split payment total
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies + '.'); 
  }
}