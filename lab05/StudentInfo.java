/*
Ethan Moscot
CSE 002
Lab 05
StudentInfo.java
This program collects data from a student, checks its expected type,
and prints results.
*/
import java.util.Scanner;
public class StudentInfo {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int courseNum; //course number
		String deptName; //name of department
		int numMeetings; //number of weekly meetings
		String startTime; //class start time
		String instructorName; //instructor's name
		int numStudents; //number of students in class

		//Checks input for student's course number
		System.out.print("Enter the course number: ");
		while(!input.hasNextInt()) {
			System.out.print("Invalid input. Reenter the course number: ");
			input.next();
		}
		courseNum = input.nextInt();

		//Checks input for department name
		System.out.print("Enter the department name: ");
		deptName = input.nextLine();
		while(!input.hasNextLine()) {
			System.out.print("Invalid input. Reenter the department name: ");
			input.next();
		}
		deptName = input.nextLine();

		//Checks input for number of weekly class meetings
		System.out.print("Enter the number of meetings per week: ");
		while(!input.hasNextInt()) {
			System.out.print("Invalid input. Reenter the number of meetings per week: ");
			input.next();
		}
		numMeetings = input.nextInt();

		//Checks input for course start time
		System.out.print("Enter the course start time: ");
		startTime = input.nextLine();
		while(!input.hasNextLine()) {
			System.out.print("Invalid input. Reenter the class start time: ");
			input.next();
		}
		startTime = input.nextLine();

		//Checks input for instructor's name
		System.out.print("Enter the instructor's name: ");
		while(!input.hasNextLine()) {
			System.out.print("Invalid input. Reenter the instructor's name: ");
			input.next();
		}
		instructorName = input.nextLine();

		//Checks input for number of students in class
		System.out.print("Enter the number of students: ");
		while(!input.hasNextInt()) {
			System.out.print("Invalid input. Reenter the number of students: ");
			input.next();
		}
		numStudents = input.nextInt();

		//Prints all data to the console
		System.out.println("The course number is " + courseNum + '.');
		System.out.println("The department name is " + deptName + '.');
		System.out.println("The number of meetings held per week is " + numMeetings + '.');
		System.out.println("The class' start time is " + startTime + '.');
		System.out.println("The instructor's name is " + instructorName + '.');
		System.out.println("The number of students is " + numStudents + '.');
	}
}