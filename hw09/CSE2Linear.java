import java.util.*;
public class CSE2Linear {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int[] grades = new int[15]; //initialize array
		for(int i = 0; i < grades.length; i++) { //Fills array with 15 students' final grades
			System.out.print("Enter CSE2 grade #" + (i + 1) + ": ");
			while(!input.hasNextInt()) { //re-prompts if an integer is not entered
				System.out.print("The inputted grade is not an integer. Reenter the final grade as an integer from 0-100: ");
				input.next();
			}
			grades[i] = input.nextInt();

			while(grades[i] < 0 || grades[i] > 100) { //re-prompts if not a valid grade
				System.out.print("The inputted grade is not a valid final grade. Reenter the final grade as an integer from 0-100: ");
				grades[i] = input.nextInt();
			}

			while(i >= 1 && grades[i] <= grades[i - 1]) { //re-prompts if grades are not entered in ascending order
				System.out.print("The inputted grade is not greater than the previous grade in the array. Reenter the grade as an integer from 0-100: ");
				grades[i] = input.nextInt();
			}
		}

		//Prompt user to enter an int to represent the grade they'd like to search for
		System.out.print("Enter the grade which you would like to search for with a binary search: ");
		int binarySearchGrade = input.nextInt();
		binarySearch(grades, binarySearchGrade); //binary search

		randomScramble(grades); //Scramble the array
		printArray(grades); //Print the newly-scrambled array

		//Prompt user to enter grade for 2nd search
		System.out.print("Enter another grade to search for using a linear search: ");
		int linearSearchGrade = input.nextInt();
		linearSearch(grades, linearSearchGrade); //linear search
	}

	//Executes binary search given a list and grade to search
	public static void binarySearch(int[] list, int grade) {
		int first = 0;
		int iteration = 0;
		int last = list.length - 1;
		int mid = (first + last) / 2;  
		while(first <= last) { //still more to search
			iteration++;
			if(list[mid] < grade) {  
				first = mid + 1;     
			} else if(list[mid] == grade) {  
				System.out.println(grade + " has been found after " + iteration + " iteration(s).");  
				break;  
			} else {  
				last = mid - 1;  
			}  
			mid = (first + last) / 2;  
		}  
		if(first > last) {  
			System.out.println(grade + " was not found after " + iteration + " iteration(s).");  
		}  
	}

	//Also used in previous homework: continually swaps 0th index with randomly-generated index
	public static void randomScramble(int[] list) {
		int counter = 0;
		while(counter <= 40) { //picked high enough number (40 in this case) to ensure decent "randomness"
			int randomIndex = (int)(Math.random() * 15);
			int temp = list[0];
			list[0] = list[randomIndex];
			list[randomIndex] = temp;
			counter++;
		}
	}

	//Conducts linear search for passed in element given an array
	public static void linearSearch(int[] list, int grade) {
		int iteration = 0;
		//boolean wasFound = false;
		//int index = 0;
		for(int i = 0; i < list.length; i++) {
			iteration++;
			if(list[i] == grade) {
				System.out.println(grade + " was found after " + iteration + " iteration(s).");
				break;
			} else if (i == list.length - 1 && list[i] != grade) {
				System.out.println(grade + " was not found after " + iteration + " iteration(s).");
			}
		}
	}

	//Prints out each element in array separated by space
	public static void printArray(int[] list) {
		for(int i = 0; i < list.length; i++) {
			System.out.print(list[i] + " ");
		}
		System.out.println();
	}
}