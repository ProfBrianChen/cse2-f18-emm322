import java.util.*;
public class RemoveElements {
	public static void main(String[] args) { //Prof. Carr's main method as provided in assignment specs
		Scanner scan = new Scanner(System.in);
		int num[] = new int[10];
		int newArray1[], newArray2[];
		int index, target;
		String answer = "";
		do {
			System.out.print("Random input 10 ints [0-9]");
			num = randomInput();
			String out = "\nThe original array: ";
			out += listArray(num);
			System.out.println(out);

			System.out.print("Enter the index: ");
			index = scan.nextInt();
			newArray1 = delete(num,index);
			String out1 = "\nThe output array: ";
			out1 += listArray(newArray1); //return a string of the form "{2, 3, -9}"  
			System.out.println(out1);

			System.out.print("Enter the target value: ");
			target = scan.nextInt();
			newArray2 = remove(num,target);
			String out2 = "\nThe output array: ";
			out2 += listArray(newArray2); //return a string of the form "{2, 3, -9}"  
			System.out.println(out2);

			System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
			answer=scan.next();
		} while(answer.equals("Y") || answer.equals("y"));
	}

	public static String listArray(int num[]) { //Prof. Carr's provided method in assignment specs
		String out = "{";
		for(int j = 0; j<num.length; j++){
			if(j > 0) {
				out += ", ";
			}
			out += num[j];
		}
		out += "} ";
		return out;
	}

	//Generates an array of 10 random integers between 0-9
	public static int[] randomInput() {
		int[] randArr = new int[10];
		for(int i = 0; i < randArr.length; i++) {
			int rand = (int)(Math.random() * 9 + 1);
			randArr[i] = rand;
		}
		return randArr;
	}

	//Creates a new array having one member fewer than list composing the same elements except for pos
	public static int[] delete(int[] list, int pos) {
		Scanner input = new Scanner(System.in);
		while(pos < 0 || pos > list.length - 1) { //checks if pos is out of bounds
			System.out.print("The position you entered is not within the array. Please re-enter a index value from 0 to " + (list.length - 1) + ": ");
			pos = input.nextInt();
		}

		//Remove index
		for(int i = pos; i < list.length - 1; i++) {
			list[i] = list[i + 1];
		}

		//Create, fill, and return new array
		int[] newArr = new int[list.length - 1];
		for(int j = 0; j < list.length - 1; j++) {
			newArr[j] = list[j];
		}
		return newArr;
	}

	//Deletes all elements equal to target, returning new list without the deleted elements
	public static int[] remove(int[] list, int target) {
		//Determine the length of the updated array that is eventually returned
		int elementsToKeep = 0;
		for (int i = 0; i < list.length; i++) {
			if(list[i] != target) {
				list[elementsToKeep++] = list[i];
			}
		}

		//Create new array and fill it with the values from the modified original array
		int[] newArr = new int[elementsToKeep];
		for(int i = 0; i < newArr.length; i++) {
			newArr[i] = list[i];
		}
		return newArr;
	}
}