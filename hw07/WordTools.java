/*
Ethan Moscot
CSE 002
10/26/18
HW 07
 */
import java.util.*;
public class WordTools {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String userTxt = sampleText();
		char choice = printMenu(input);
		while(choice != 'q') { //runs while user has not quit
			while(choice != 'c' && choice != 'w' && choice != 'f' && choice != 'r' && choice != 's') { //protects against invalid input
				System.out.println("You have entered an invalid character. Please reselect below.");
				choice = printMenu(input);
			}
			if(choice == 'c') { //whitespace
				System.out.println("Non whitespace characters: " + getNumOfNonWSCharacters(userTxt));
			} else if(choice == 'w') { //word counter
				System.out.println("Number of words: " + getNumOfWords(userTxt));
			} else if(choice == 'f') { //find phrase in string
				System.out.print("Enter the string you'd like to find: ");
				String strToBeFound = input.nextLine();
				System.out.println("Number of instances: " + findText(strToBeFound, userTxt));
			} else if(choice == 'r') { //replace exclamation points with periods
				System.out.println("Text with !\'s replaced: " + replaceExclamation(userTxt));
			} else if(choice == 's') { //shorten double space
				shortenSpace(userTxt);
				System.out.println("Singled space text: " + userTxt);
			}
			System.out.println();
			choice = printMenu(input);
		}
	}

	//Prompts the user to enter a string of their choosing, stores this string, and outputs it
	public static String sampleText() {
		Scanner txt = new Scanner(System.in);
		System.out.print("Enter sample text: ");
		String userText = txt.nextLine();
		return userText;
	}

	//Outputs a menu of user options for analyzing/editing the string, and returns the user's entered menu option
	public static char printMenu(Scanner input) {
		System.out.println("MENU");
		System.out.println("c - Number of non-whitespace characters");
		System.out.println("w - Number of words");
		System.out.println("f - Find text");
		System.out.println("r - Replace all !\'s");
		System.out.println("s - Shorten spaces");
		System.out.println("q - Quit");

		System.out.print("Choose an option: ");
		char option = input.nextLine().charAt(0);
		return option;
	}

	//Returns the number of characters in the String, excluding all whitespace
	public static int getNumOfNonWSCharacters(String str) {
		int nonSpace = 0;
		for(int i = 0; i < str.length(); i++) {
			if(str.charAt(i) != ' ') {
				nonSpace++;
			}
		}
		return nonSpace;
	}

	//Returns the number of words in the string
	public static int getNumOfWords(String str) {
		int wordCount = 0;
		boolean isWord = false;
		for(int i = 0; i < str.length(); i++) {
			if(Character.isLetter(str.charAt(i)) && i != str.length() - 1) {
				isWord = true;
			} else if(!Character.isLetter(str.charAt(i)) && isWord) {
				wordCount++;
				isWord = false;
			} else if(Character.isLetter(str.charAt(i)) && i == str.length() - 1) {
				wordCount++;
			}
		}
		return wordCount;
	}

	//Returns the number of instances a word or phrase is found in the string
	public static int findText(String strToFind, String str) {
		int counter = 0;
		for(int i = 0; i < str.length(); i++) {
			if(str.charAt(i) == strToFind.charAt(0)) {
				int k = 0;
				boolean doesMatch = true; //checks if string and user string match
				for(int j = i; j - i < strToFind.length(); k++, j++) {
					if(str.charAt(j) != strToFind.charAt(k)) {
						doesMatch = false;
					}
				}
				if(doesMatch = true) {
					counter++;
				}
			}
		}
		return counter;
	}

	//Returns a string which replaces each '!' character in the string with a '.' character
	public static String replaceExclamation(String str) {
		for(int i = 0; i < str.length(); i++) {
			if(str.charAt(i) == '!') {
				str = str.replace(str.charAt(i), '.');
			}
		}
		return str;
	}

	//Returns a string that replaces all sequences of 2 or more spaces with a single space
	public static void shortenSpace(String str) {
		str.replaceAll("\\s+", "  ");
	}
}
