public class WelcomeClass {
  public static void main(String[] args) {
    //Welcome message
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    
    //Lehigh ID top row formatting
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    //Lehigh username
    System.out.println("<-E--M--M--3--2--2->");
    //Bottom row formatting
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
                       
    //Tweet-length autobiographic statement
    System.out.println("\nI am from Los Angeles and I am planning on studying computer science at Lehigh." +
                       "\nOutside of the classroom, I enjoy listening to John Mayer, playing tennis, biking, and reading a good book.");
  }
}