/*
Ethan Moscot
CSE 002
HW 03
This program converts the inches of hurricane precipitation into cubic miles.
*/
import java.util.Scanner;
public class Convert {
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    //Receive user input for acres of land
    System.out.print("Enter the number of acres affected: ");
    double numAcres = input.nextDouble();
    //Receive user input for inches of rain
    System.out.print("Enter the average amount of rain in inches: ");
    double inchesRain = input.nextDouble();
    double acreInches = numAcres * inchesRain; //Multiply acres and inches to combine the two into acre-inches
    
    //Convert acre inches to gallons according to https://converterin.com/volume/acre-inch-to-gallon-us-liquid.html.
    double numGallons = (0.028316847 * 3630) / (0.000016387064 * 231) * acreInches;
    //Convert gallons to cubic miles according to https://www.convertunits.com/from/gallon+[US,+liquid]/to/cubic+mile.
    double cubicMiles = numGallons / 1101117147428.6;
    
    //Display results of acre inches to cubic miles conversion
    System.out.println(acreInches + " acre inches of rain converted to cubic miles is " + cubicMiles + " cubic miles.");
  }
}