/*
Ethan Moscot
CSE 002
HW #03 Part 2
This program computes the volume of a pyramid when provided with dimensions as input.
*/
import java.util.Scanner;
public class Pyramid {
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    double squareSide; //Stores given side
    double height; //Stores given height
    double volume; //Stores volume calculation
    //Receive and store user input for square side length
    System.out.print("Enter the length of the pyramid's square side: ");
    squareSide = input.nextDouble();
    //Receive and store user input for pyramid's height
    System.out.print("Enter the height of the pyramid: ");
    height = input.nextDouble();
   
    //Calculate pyramid volume given dimensions of square side and height according to https://keisan.casio.com/exec/system/1223352513.
    volume = (Math.pow(squareSide, 2.0) * height) / 3;
    //Display result to the console
    System.out.println("The volume inside of the pyramid is " + volume + ".");
  }
}