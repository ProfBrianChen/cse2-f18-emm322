/*
Ethan Moscot
CSE 002 Lab 06
10/12/18
*/
import java.util.Scanner;
public class PatternB {
	public static void main(String[] args) {
		int numRows;
		Scanner input = new Scanner(System.in);
		//Accepts user input to generate the number of rows and checks if input is an integer
		System.out.print("Enter the number of rows (between 1-10): ");
		while(!input.hasNextInt()) {
			System.out.print("The input is not an integer. Reenter the number of rows as a number between 1-10: ");
			input.next();
		}
		numRows = input.nextInt();
		//Checks if input is between the range 1-10
		while(numRows < 1 || numRows > 10) {
			System.out.print("The input is not within 1-10. Reenter the number of rows from 1-10: ");
			numRows = input.nextInt();
		}
		
		for(int i = numRows; i >= 1; i--) { //loops through number of rows
			for(int j = 1; j <= i; j++) { //nested loop represents digits to display
				System.out.print(j + " ");
			}
			System.out.println();
		}
	}
}