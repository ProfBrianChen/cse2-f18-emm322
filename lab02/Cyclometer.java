/*
Ethan Moscot
CSE 002 Lab 02
This program simulates a bicycle's cyclometer in order to measure data such as speed and distance. 
*/
public class Cyclometer {
  public static void main(String[] args) {
    int tripOneSecs = 1000; //trip one time in seconds
    int tripTwoSecs = 1560; //trip two time in seconds
    int tripOneRotations = 2137; //trip one front wheel rotations
    int tripTwoRotations = 2456; //trip two front wheel rotations
    double distanceTrip1, distanceTrip2, totalDistance; //individual trip and total trip distances
    //Useful constants, measurements and conversions
    double PI = 3.14159;
    int ftPerMile = 5280; //5280 ft in 1 mile
  	int inchPerFt = 12; //12 inches per feet
  	int secsPerMin = 60; //60 secs in 1 min
    double wheelDiameter = 27.0; //diameter of bike wheel
    
    //Time and rotation summary of the two trips
    System.out.println("Trip 1 took " + (tripOneSecs / secsPerMin) + " minutes and had " + tripOneRotations + " rotations.");
    System.out.println("Trip 2 took " + (tripTwoSecs / secsPerMin) + " minutes and had " + tripTwoRotations + " rotations.");
    
    distanceTrip1 = tripOneRotations * wheelDiameter * PI; //Converts trip one distance to inches 
    distanceTrip1 /= inchPerFt * ftPerMile; //Converts trip two distance to miles
    distanceTrip2 = tripTwoRotations * wheelDiameter * PI; //Converts trip two distance to inches
    distanceTrip2 /= inchPerFt * ftPerMile; //Conerts trip two distance to miles
    totalDistance = distanceTrip1 + distanceTrip2; //Computes total distance of two trips
    
    //Prints trip cyclometer info to the console
    System.out.println("Trip 1 was " + distanceTrip1 + " miles.");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles.");
    System.out.println("The total distance of the two trips was " + totalDistance + " miles.");
  }
}